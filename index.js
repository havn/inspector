const os = require('os')
const serialNumber = require('serial-number')
const ps = require('current-processes')

module.exports.getIds = function(cb){
  var result = {};
  serialNumber(function(err, data){
    if(err) { cb(err); return; }
    result.sn = data
    serialNumber.preferUUID = true
    serialNumber(function(err, data){
      if(err) { cb(err); return; }
      result.guid = data
      cb(null, result)
    }.bind(this));
  }.bind(this));
}

module.exports.getSystemInfo = function(cb){
  cb(null, {
    arch: os.arch(),
    cpus: os.cpus(),
    endianness: os.endianness(),
    hostname: os.hostname(),
    platform: os.platform(),
    release: os.release(),
    type: os.type(),
    tmpdir: os.tmpdir(),
    homedir: os.homedir(),
    userInfo: os.userInfo()
  })
}

module.exports.getPerformanceInfo = function(cb){
  cb(null, {
    freemem: os.freemem(),
    totalmem: os.totalmem(),
    loadavg: os.loadavg(),
    networkInterfaces: os.networkInterfaces(),
    uptime: os.uptime()
  })
}

module.exports.getRunning = function(cb){
  ps.get(cb)
}
